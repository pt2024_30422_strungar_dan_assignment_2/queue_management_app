package ro.tuc.tp.business_logic;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ro.tuc.tp.model.Server;
import ro.tuc.tp.model.Task;

public class Scheduler {

    private List<Server> servers;
    private int maxNoServers;
    private int maxTasksPerServer;
    private Strategy strategy;
    private ExecutorService executorService;

    public Scheduler(int maxNoServers, int maxTasksPerServer) {
        this.maxNoServers = maxNoServers;
        this.maxTasksPerServer = maxTasksPerServer;
        servers = new ArrayList<>(maxTasksPerServer);
        executorService = Executors.newFixedThreadPool(maxNoServers);

        for (int i = 0; i < maxNoServers; i++) {
            Server server = new Server();
            servers.add(server);
            executorService.submit(server);
        }
    }

    public void changeStrategy(SelectionPolicy selectionPolicy) {
        if (selectionPolicy == SelectionPolicy.SHORTEST_QUEUE) {
            strategy = new ShortestQueueStrategy();
        } else {
            strategy = new TimeStrategy();
        }
    }

    public void dispatchTask(Task t) {
        strategy.addTask(servers, t);
    }

    public List<Server> getServers() {
        return servers;
    }

    public void shutdown() {
        executorService.shutdown();
    }
    public List<Task> getQueuedTasks() {
        List<Task> queuedTasks = new ArrayList<>();
        for (Server server : this.getServers()) {
            queuedTasks.addAll(server.getTasks());
        }
        return queuedTasks;
    }
}
