package ro.tuc.tp.business_logic;

public enum SelectionPolicy {
    SHORTEST_QUEUE, SHORTEST_TIME;
}
