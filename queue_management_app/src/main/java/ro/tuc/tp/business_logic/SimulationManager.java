package ro.tuc.tp.business_logic;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import ro.tuc.tp.gui.InputFrame;
import ro.tuc.tp.gui.SimulationFrame;
import ro.tuc.tp.model.InputDTO;
import ro.tuc.tp.model.Server;
import ro.tuc.tp.model.Task;

public class SimulationManager implements Runnable{
    private int timeLimit;
    private int maxProcessingTime;
    private int minProcessingTime;
    private int maxArrivalTime;
    private int minArrivalTime;
    private int numberOfServers;
    private int numberOfTasks;
    private SelectionPolicy selectionPolicy;
    private final FileWriter fileWriter;
    private double totalWaitingTime;
    private int addressedTasks;

    private Scheduler scheduler;
    private SimulationFrame frame;
    private ArrayList<Task> tasks;
    private int[] arrivalsPerHour;

    private boolean started = false;

    public SimulationManager(InputDTO inputDTO) {
        this.timeLimit = inputDTO.timeLimit();
        this.maxProcessingTime = inputDTO.maxProcessingTime();
        this.minProcessingTime = inputDTO.minProcessingTime();
        this.maxArrivalTime = inputDTO.maxArrivalTime();
        this.minArrivalTime = inputDTO.minArrivalTime();
        this.numberOfServers = inputDTO.numberOfServers();
        this.numberOfTasks = inputDTO.numberOfTasks();
        this.selectionPolicy = inputDTO.selectedPolicy();
        this.totalWaitingTime = 0;
        this.addressedTasks = 0;
        this.scheduler = new Scheduler(numberOfServers, numberOfTasks);
        scheduler.changeStrategy(selectionPolicy);
        arrivalsPerHour = new int[timeLimit];
        generateNRandomTasks();
        started = true;
        try {
           fileWriter = new FileWriter("queue_management_app/src/main/resources/log_test3.txt");
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Could not create file writer.");
        }
    }

    private void generateNRandomTasks() {
        tasks = new ArrayList<>();
        Random random = new Random();
        for(int i = 0; i < numberOfTasks; i++) {
            Task t = new Task(i + 1, random.nextInt(maxArrivalTime - minArrivalTime) + minArrivalTime, random.nextInt(maxProcessingTime - minProcessingTime) + minProcessingTime);
            tasks.add(t);
        }
    }

    private void printQueuesToFile(int currentTime, FileWriter fileWriter) {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("Time: " + currentTime + "\n");
            for (int i = 0; i < numberOfServers; i++) {
                bufferedWriter.write("Queue " + i + ":\n");
                for (Task task : scheduler.getServers().get(i).getTasks()) {
                    bufferedWriter.write("(" + task.getID() + ", " + task.getArrivalTime() + ", " + task.getServiceTime() + ") ");
                }
                bufferedWriter.write("\n");
            }
            bufferedWriter.write("\n");
            bufferedWriter.flush(); // Flush the buffer
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Error occurred while writing to log file: " + e.getMessage());
        }
    }
    

    public void closeFileWriter() {
        try {
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void clearConsole() {
        System.out.print("\033[H\033[2J"); // ANSI escape code to clear the console
        System.out.flush();
    }

    public boolean isSimulationStarted() {
        return started;
    }
    @Override
    public void run() {
    int currentTime = 1;
    while (currentTime <= timeLimit) {
        Iterator<Task> iterator = tasks.iterator();
        while (iterator.hasNext()) {
            Task t = iterator.next();
            if(t.getArrivalTime() == currentTime) {
                scheduler.dispatchTask(t);
                addressedTasks++;
                totalWaitingTime += t.getWaitingTime();
                arrivalsPerHour[currentTime - 1] = findLongestQueue();
                //System.out.println(findLongestQueue());
                iterator.remove();
            }
        }
        //System.out.println("Time: " + currentTime);
        printQueuesToFile(currentTime, fileWriter);
        frame.updateQueueVisuals(scheduler);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        currentTime++;
    }
    scheduler.shutdown();
    try {
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write("Average waiting time: " + computeAverageWaitingTime() + "\n");
        bufferedWriter.write("Peak hour: " + ((findPeakHour())+1) + "\n");
        bufferedWriter.write("Simulation finished.");
        bufferedWriter.flush(); // Flush the buffer
    } catch (IOException e) {
        e.printStackTrace();
        System.err.println("Error occurred while writing to log file: " + e.getMessage());
    }
    closeFileWriter();
}

    public static void main(String[] args) {
        InputFrame inputFrame = new InputFrame();
        while(!inputFrame.isSimulationStarted()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        SimulationManager gen = new SimulationManager(inputFrame.getInputDTO());
        // for(Task t : gen.tasks) {
        //     System.out.println(t);
        // }
        while(!gen.isSimulationStarted()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        gen.frame = new SimulationFrame(gen.numberOfServers, gen.scheduler, gen.timeLimit);
        Thread t = new Thread(gen);
        t.start();
    }

    private int findPeakHour() {
        int maxHour = 0;
        int maxArrivals = arrivalsPerHour[0];
        for (int i = 1; i < arrivalsPerHour.length; i++) {
            if (arrivalsPerHour[i] > maxArrivals) {
                maxHour = arrivalsPerHour[i];
            }
        }
        // for(int arrivalPerHour : arrivalsPerHour){
        //     System.out.println(arrivalPerHour);
        // }
        return maxHour;
    }

    private int findLongestQueue(){
        int top = 0;
        for(Server server : scheduler.getServers()){
            if(top < server.getTasks().size()){
                top = server.getTasks().size();
            }
        }
        return top;
    }
    
    // Method to compute average waiting time
    private double computeAverageWaitingTime() {
        return totalWaitingTime/addressedTasks;
    }

    
}
