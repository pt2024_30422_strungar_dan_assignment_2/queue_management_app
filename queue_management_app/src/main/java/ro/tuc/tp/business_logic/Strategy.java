package ro.tuc.tp.business_logic;

import java.util.List;

import ro.tuc.tp.model.Server;
import ro.tuc.tp.model.Task;

public interface Strategy {

    public void addTask(List<Server> servers, Task t);
    
}
