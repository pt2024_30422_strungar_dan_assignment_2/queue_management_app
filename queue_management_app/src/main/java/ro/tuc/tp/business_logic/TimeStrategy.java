package ro.tuc.tp.business_logic;

import java.util.List;

import ro.tuc.tp.model.Server;
import ro.tuc.tp.model.Task;

public class TimeStrategy implements Strategy{

    @Override
    public void addTask(List<Server> servers, Task t) {
        int min = Integer.MAX_VALUE;
        Server s = null;
        for(Server server : servers) {
            if(server.getWaitingPeriod().get() < min) {
                min = server.getWaitingPeriod().get();
                s = server;
            }
        }
        s.addTask(t);
    }
}
