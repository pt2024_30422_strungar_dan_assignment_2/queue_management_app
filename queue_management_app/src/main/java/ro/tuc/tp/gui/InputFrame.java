package ro.tuc.tp.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import ro.tuc.tp.business_logic.SelectionPolicy;
import ro.tuc.tp.model.InputDTO;

public class InputFrame extends JFrame{
    private JTextField timeLimitField;
    private JTextField maxProcessingTimeField;
    private JTextField minProcessingTimeField;
    private JTextField numberOfServersField;
    private JTextField numberOfTasksField;
    private JTextField maxArrivalTimeField;
    private JTextField minArrivalTimeField;
    private SelectionPolicy selectedPolicy; // To store the selected policy
    private boolean simulationStarted = false;
    private InputDTO inputDTO;

    public InputFrame() {
        setTitle("Simulation Parameters");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(750, 300);
        // Initialize components
        timeLimitField = new JTextField(10);
        maxProcessingTimeField = new JTextField(10);
        minProcessingTimeField = new JTextField(10);
        numberOfServersField = new JTextField(10);
        numberOfTasksField = new JTextField(10);
        maxArrivalTimeField = new JTextField(10);
        minArrivalTimeField = new JTextField(10);

        // Create labels
        JLabel timeLimitLabel = new JLabel("Time Limit:");
        JLabel numberOfTasksLabel = new JLabel("Number of Clients:");
        JLabel numberOfServersLabel = new JLabel("Number of Queues:");
        JLabel minArrivalTimeLabel = new JLabel("Min Arrival Time:");
        JLabel maxArrivalTimeLabel = new JLabel("Max Arrival Time:");
        JLabel minProcessingTimeLabel = new JLabel("Min Processing Time:");
        JLabel maxProcessingTimeLabel = new JLabel("Max Processing Time:");
        JLabel selectedPolicyLabel = new JLabel("Selected Policy:");

        String[] options = {"Shortest Queue", "Shortest Time"};

        JComboBox<String> policyComboBox = new JComboBox<>(options);

        policyComboBox.setSelectedIndex(0);

        policyComboBox.addActionListener(e -> {
            JComboBox cb = (JComboBox)e.getSource();
            String selectedPolicyString = (String)cb.getSelectedItem();
            if(selectedPolicyString.equals("Shortest Queue")) {
                selectedPolicy = SelectionPolicy.SHORTEST_QUEUE;
            } else {
                selectedPolicy = SelectionPolicy.SHORTEST_TIME;
            }
        });

        // Create button to start simulation
        JButton startButton = new JButton("Start Simulation");

        // Create panel to hold components
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(0, 2));
        panel.add(timeLimitLabel);
        panel.add(timeLimitField);
        panel.add(numberOfTasksLabel);
        panel.add(numberOfTasksField);
        panel.add(numberOfServersLabel);
        panel.add(numberOfServersField);
        panel.add(minArrivalTimeLabel);
        panel.add(minArrivalTimeField);
        panel.add(maxArrivalTimeLabel);
        panel.add(maxArrivalTimeField);
        panel.add(minProcessingTimeLabel);
        panel.add(minProcessingTimeField);
        panel.add(maxProcessingTimeLabel);
        panel.add(maxProcessingTimeField);
        panel.add(selectedPolicyLabel); 
        panel.add(policyComboBox);
        // Add label for selected policy
        

        startButton.addActionListener(e -> {
            try {
                int timeLimit = Integer.parseInt(timeLimitField.getText());
                int maxProcessingTime = Integer.parseInt(maxProcessingTimeField.getText());
                int minProcessingTime = Integer.parseInt(minProcessingTimeField.getText());
                int numberOfServers = Integer.parseInt(numberOfServersField.getText());
                int numberOfTasks = Integer.parseInt(numberOfTasksField.getText());
                int maxArrivalTime = Integer.parseInt(maxArrivalTimeField.getText());
                int minArrivalTime = Integer.parseInt(minArrivalTimeField.getText());

                // Create InputDTO object with simulation parameters
                this.inputDTO = new InputDTO(timeLimit, maxProcessingTime, minProcessingTime, maxArrivalTime, minArrivalTime, numberOfServers, numberOfTasks, selectedPolicy);
                this.setState(JFrame.ICONIFIED); // Minimize the current frame\
                simulationStarted = true;

            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(this, "Invalid input. Please complete all the fields with valid numbers!");
            }
        
        });
        
        // Add panel and button to frame
        add(panel, BorderLayout.CENTER);
        add(startButton, BorderLayout.SOUTH);
        setVisible(true);
    }

    public boolean isSimulationStarted() {
        return simulationStarted;
    }

    public InputDTO getInputDTO() {
        return inputDTO;
    }
}
