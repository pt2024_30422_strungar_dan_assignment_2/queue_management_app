package ro.tuc.tp.gui;

import javax.swing.*;

import ro.tuc.tp.business_logic.Scheduler;
import ro.tuc.tp.model.Task;

import java.awt.*;
import java.util.ArrayList;
import java.util.Queue;

public class SimulationFrame extends JFrame {
    private JPanel superPanel;
    private int elapsedTime;
    private int simulationTime;
    private ArrayList<JPanel> subPanels;

    public SimulationFrame(int numberOfServers, Scheduler scheduler, int simulationTime) {
        setTitle("Simulation");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.elapsedTime = 0; // Initialize elapsed time to 0
        this.simulationTime = simulationTime;
        this.subPanels = new ArrayList<>();

        JPanel headerPanel = new JPanel();
        headerPanel.setLayout(new BorderLayout());

        // Create a JLabel to display the current time
        JLabel timeLabel = new JLabel();
        timeLabel.setFont(new Font("Arial", Font.PLAIN, 40));
        updateTimeLabel(timeLabel); // Update the time label initially
        headerPanel.add(timeLabel, BorderLayout.CENTER);

        // Add the header panel to the top of the frame
        getContentPane().add(headerPanel, BorderLayout.NORTH);
        // Create the super panel
        superPanel = new JPanel();
        superPanel.setLayout(new BoxLayout(superPanel, BoxLayout.Y_AXIS));
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();
        setSize(screenSize.width, screenSize.height-40);
        setResizable(false);

        // Create a JScrollPane
        JScrollPane scrollPane = new JScrollPane(superPanel);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        // Add components to the super panel
        for (int i = 1; i <= numberOfServers; i++) {
            JPanel subPanel = new JPanel();
            subPanels.add(subPanel);
            subPanel.setLayout(new BorderLayout());

            JLabel label = new JLabel();
            subPanel.add(label, BorderLayout.WEST);

            // Load and add the image to the right side
            ImageIcon icon = new ImageIcon("queue_management_app/src/main/resources/mecu_mic.png"); // Adjust the path
            JLabel imageLabel = new JLabel(icon);
            imageLabel.setText("MEC " + i);
            imageLabel.setVerticalTextPosition(JLabel.TOP);
            imageLabel.setHorizontalTextPosition(JLabel.CENTER);
            imageLabel.setSize(icon.getIconWidth(), icon.getIconHeight());
            subPanel.add(imageLabel, BorderLayout.EAST);

            // Set a fixed height for each sub-panel
            //subPanel.setPreferredSize(new Dimension(superPanel.getWidth(), 200)); // Adjust the height as needed

            superPanel.add(subPanel);
        }

        // Add the JScrollPane to the frame
        getContentPane().add(scrollPane);

        // Center the frame on the screen
        setLocationRelativeTo(null);
        
        new Timer(1000, e -> {
            updateTimeLabel(timeLabel);
            elapsedTime++;
            if (elapsedTime >= simulationTime) {
                ((Timer)e.getSource()).stop(); // Stop the timer when the simulation time limit is reached
            }
        }).start();

        setVisible(true);
    }
    private void updateTimeLabel(JLabel timeLabel) {
        timeLabel.setText("Elapsed Time: " + elapsedTime + "s / " + simulationTime + "s"); // Display elapsed time as an integer
    }

    public void updateQueueVisuals(Scheduler scheduler) {
        for (int i = 0; i < subPanels.size(); i++) {
            JPanel subPanel = subPanels.get(i);
            subPanel.removeAll(); // Clear existing components
            
            // Determine the number of rows based on the queue size
            int numCols = scheduler.getServers().get(i).getTasks().size() + 2; // +2 for the "house" and "MEC"
            
            // Use GridBagLayout with GridBagConstraints to set cell dimensions and alignment
            subPanel.setLayout(new GridBagLayout());
            
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.anchor = GridBagConstraints.LINE_START; // Align components to the left
            gbc.gridx = 0; // Start from the first column
            gbc.gridy = 0; // Start from the first row
            
            // Load and add the "MEC" image to the first cell
            ImageIcon mecIcon = new ImageIcon("queue_management_app/src/main/resources/mecu_mic.png"); // Adjust the path
            JLabel mecLabel = new JLabel(mecIcon);
            mecLabel.setText("MEC " + (i + 1));
            mecLabel.setVerticalTextPosition(JLabel.TOP);
            mecLabel.setHorizontalTextPosition(JLabel.CENTER);
            subPanel.add(mecLabel, gbc);
            
            // Add car images to cells next
            gbc.gridx++; // Move to the next column
            Queue<Task> queue = scheduler.getServers().get(i).getTasks();
            for (Task task : queue) {
                // Load and add the car image
                ImageIcon carIcon = new ImageIcon("queue_management_app/src/main/resources/car_mic.png"); // Adjust the path
                JLabel carLabel = new JLabel(carIcon);
                carLabel.setText("ID: " + task.getID() + " AT: " +  task.getArrivalTime() + " ST: "+ task.getServiceTime());
                carLabel.setVerticalTextPosition(JLabel.TOP);
                carLabel.setHorizontalTextPosition(JLabel.CENTER);
                subPanel.add(carLabel, gbc);
                gbc.gridx++; // Move to the next column
            }
            
            // Load and add the "house" image to the last cell
            ImageIcon houseIcon = new ImageIcon("queue_management_app/src/main/resources/house.png"); // Adjust the path
            JLabel houseLabel = new JLabel(houseIcon);
            subPanel.add(houseLabel, gbc);
            
            // Repaint the sub panel
            subPanel.revalidate();
            subPanel.repaint();
        }
    }    
}
