package ro.tuc.tp.model;

import ro.tuc.tp.business_logic.SelectionPolicy;

public record InputDTO(int timeLimit, 
                       int maxProcessingTime, 
                       int minProcessingTime, 
                       int maxArrivalTime, 
                       int minArrivalTime, 
                       int numberOfServers, 
                       int numberOfTasks, 
                       SelectionPolicy selectedPolicy) {
}
