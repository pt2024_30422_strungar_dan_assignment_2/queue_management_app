package ro.tuc.tp.model;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable{
    BlockingQueue<Task> tasks;
    AtomicInteger waitingPeriod;

    public Server() {
        this.tasks= new LinkedBlockingQueue<>();
        this.waitingPeriod = new AtomicInteger(0);
    }

    public void addTask(Task t) {
        tasks.add(t);
        waitingPeriod.addAndGet(t.getServiceTime());
        t.setWaitingTime(waitingPeriod.get());
    }

    public BlockingQueue<Task> getTasks() {
        return tasks;
    }

    public AtomicInteger getWaitingPeriod() {
        return waitingPeriod;
    }
    
    @Override
    public void run(){
        while(true){
            if(tasks.size() > 0){
                Task t = tasks.peek();
                while(t.getServiceTime() > 0){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    waitingPeriod.getAndDecrement();
                    t.setServiceTime(t.getServiceTime() - 1);
                }
                tasks.poll();
            }
        }
    }
}
