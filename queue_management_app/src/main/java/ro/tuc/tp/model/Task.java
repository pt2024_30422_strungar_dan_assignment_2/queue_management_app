package ro.tuc.tp.model;

public class Task {
    private int ID;
    private int arrivalTime;
    private int serviceTime;
    private int waitingTime;

    public Task(int iD, int arrivalTime, int serviceTime) {
        ID = iD;
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
        this.waitingTime = 0;
    }

    public int getID() {
        return ID;
    }
    public void setID(int iD) {
        ID = iD;
    }
    public int getArrivalTime() {
        return arrivalTime;
    }
    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
    public int getServiceTime() {
        return serviceTime;
    }
    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
    }

    @Override
    public String toString() {
        return "Task [ID=" + ID + ", arrivalTime=" + arrivalTime + ", serviceTime=" + serviceTime + "]";
    }
}
